const navMenu = document.querySelector(".header__navigation-menu-button");
const navMenuIcon = document.querySelector(".header__navigation-menu-button-icon");
const menuList = document.querySelector(".header__navigation-menu");


navMenu.addEventListener("click", (event) => {
    navMenuIcon.classList.toggle("mdi-menu");
    navMenuIcon.classList.toggle("mdi-close");
    menuList.classList.toggle("hide");
});