class Trello {
    constructor() {
        this._addnewColumn = document.getElementById('add-new-column');
        this._addnewColumn.addEventListener('click', () => {
            this.addNewList();
        });
        this._idCounter = 1;

        this.addNewList();
    }

    addNewList() {
        const newColumn = document.createElement('div');
        newColumn.classList.add('task-column');
        this._addnewColumn.before(newColumn);

        const taskColumnTitle = document.createElement('div');
        taskColumnTitle.classList.add('task-column__title');
        newColumn.append(taskColumnTitle);

        const taskColumnTitleText = document.createElement('input');
        taskColumnTitleText.classList.add('task-column__name');
        taskColumnTitleText.type = 'text';
        taskColumnTitleText.value = 'To do';
        taskColumnTitle.append(taskColumnTitleText);

        const sortBtn = document.createElement('input');
        sortBtn.type = 'button';
        sortBtn.value = 'Sort';
        sortBtn.classList.add('task-column__sort-by-names');
        taskColumnTitle.append(sortBtn);
        sortBtn.addEventListener('click', this.sortByText);

        const taskList = document.createElement('ul');
        taskList.classList.add('task-column__list');
        taskList.addEventListener('drop', this.drop);
        taskList.addEventListener("dragover", this.allowDrop);
        newColumn.append(taskList);

        const addNewTaskBtn = document.createElement('input');
        addNewTaskBtn.type = "button";
        addNewTaskBtn.value = "+ add a card...";
        addNewTaskBtn.classList.add("task-column__add-new-btn");
        newColumn.append(addNewTaskBtn);
        addNewTaskBtn.addEventListener('click', () => {
            this.renderCard(taskList);
        });
    }

    renderCard(insert, text = '') {
        const newTask = document.createElement('li');
        newTask.classList.add('task-card');
        newTask.id = `task-${this._idCounter}`;
        this._idCounter++;
        newTask.draggable = true;
        insert.append(newTask);
        newTask.addEventListener("dragstart", this.drag);

        const taskInput = document.createElement('textarea');
        taskInput.classList.add('task-card__message');
        taskInput.placeholder = 'Edit task...';
        taskInput.innerText = text;
        newTask.append(taskInput);

        taskInput.addEventListener('input', () => {
            taskInput.style.height = "5px";
            taskInput.style.height = (taskInput.scrollHeight) + "px";
        })
    }

    sortByText(event) {
        const parentColumn = event.target.parentElement.parentElement;
        const unsortedListItems = parentColumn.querySelectorAll('.task-card');
        if (unsortedListItems.length === 0) {
            return false;
        }

        const parentWrapper = unsortedListItems[0].parentElement;
        const unsortedArray = [];

        unsortedListItems.forEach((item) => {
            unsortedArray.push(item);
            parentWrapper.removeChild(item);
        });

        const sortedList = unsortedArray.sort((a, b) => {
            let first = a.firstElementChild.value.toLowerCase();
            let second = b.firstElementChild.value.toLowerCase();
            if (first > second) return 1;
            if (first < second) return -1;
            return 0;
        });

        sortedList.forEach(item => {
            parentWrapper.append(item);
        });
    }

    drag(event) {
        event.dataTransfer.setData("text", event.target.id);
    }

    drop(event) {
        event.preventDefault();
        const data = event.dataTransfer.getData("text");

        if (event.target.classList.contains('task-card')) {
            event.target.before(document.getElementById(data))
        } else if (event.target.classList.contains('task-card__message')) {
            event.target.parentElement.after(document.getElementById(data))
        } else {
            event.target.appendChild(document.getElementById(data));
        }
    }

    allowDrop(event) {
        event.preventDefault();
    }
}

new Trello();