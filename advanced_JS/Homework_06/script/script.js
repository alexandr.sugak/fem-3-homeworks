let characters = [];

$.ajax({
    url: "https://swapi.co/api/people/",
    dataType: 'json',
    success: function (data) {
        characters = characters.concat(data.results);
        if (data.next !== null) {
            nestedInquiry(data.next);
        }
    },
    error: function (data) {
        console.log(data);
    }
});

function nestedInquiry(link) {
    $.ajax({
        url: link,
        dataType: 'json',
        success: function (data) {
            characters = characters.concat(data.results);
            if (data.next !== null) {
                nestedInquiry(data.next);
            } else {
                console.log(characters);
                collect();
            }
        },
        error: function (data) {
            console.log(data);
        }
    })
}

async function collect() {
    const elementsList = [];

    const getHomeworld = function (link) {
        return new Promise((resolve, reject) => {
            $.get(link, function (data) {
                if (data.name === undefined || data.name === null) {
                    reject("NULL")
                } else {
                    resolve(data.name);
                }
            }, 'json');
        });
    };

    for (let i = 0; i < characters.length; i++) {
        const element = {
            name: characters[i].name,
            gender: characters[i].gender,
        };
        if (typeof characters[i].starships !== 'undefined') {
            if (characters[i].starships.length > 0) {
                element.starships = characters[i].starships;
            }
        }
        try {
            element.homeworld = await getHomeworld(characters[i].homeworld);
        } catch (error) {
            console.log(`Error: ${error}`)
        }
        elementsList.push(element);
    }
    console.log('complete');
    render(elementsList);
}


function render(elementsList) {
    $('#wait').remove();
    const list = $('#list');
    elementsList.forEach((item) => {
        const listLi = $('<li></li>');
        list.append(listLi);
        const listUl = $('<ul></ul>');
        listLi.append(listUl);

        listUl.append(`<li>Name: ${item.name}<li>`);
        listUl.append(`<li>Gender: ${item.gender}</li>`);
        listUl.append(`<li>Homeworld: ${item.homeworld}</li>`);

        if (item.starships) {
            const starshipsLi = $(`<li></li>`);
            listUl.append(starshipsLi);
            const starships = $(`<button class="ship-button">Starships</button>`);
            starshipsLi.append(starships);
            starships.click((event) => {
                event.target.remove();
                starshipsLi.append('<h3>Manned ships:</h3>')
                const starshipsUl = $(`<ul></ul>`);
                starshipsLi.append(starshipsUl);
                item.starships.forEach((item) => {
                    $.get(item, function (data) {
                        starshipsUl.append(`<li>${data.name}</li>`);
                        if (data.name === "Millennium Falcon") {
                            starshipsUl.after('<h2>Han shot first!</h2>')
                        }
                    })
                })
            })
        }
    })
}