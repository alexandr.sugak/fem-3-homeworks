const request = new XMLHttpRequest();
request.open('GET', 'https://swapi.co/api/films/');
request.responseType = 'json';
request.send();

request.onload = (() => {
    if (request.status !== 200) {
        console.log(`Error: ${request.status}: ${request.statusText}`);
    } else {
        const response = request.response;
        const result = response.results;
        console.log(result);
        render(result)
    }
});


function render(films) {
    const wrapper = document.getElementById('list');
    films.forEach((item, index) => {
        const episode = document.createElement('li');
        episode.classList.add('episode-item');
        episode.innerHTML = `<ul class="episode">
<li>Episode: ${item.episode_id}</li>
<li>Title: ${item.title}</li>
<li>Crawl: ${item.opening_crawl}</li>
</ul>`;
        wrapper.append(episode);
        const charactersBtn = document.createElement('input');
        charactersBtn.type = 'button';
        charactersBtn.classList.add('character-button');
        charactersBtn.value = 'Characters list';
        episode.append(charactersBtn);

        charactersBtn.addEventListener('click', () => {
            characterList(item.characters, episode.firstChild);
            charactersBtn.style.display = 'none';
        });
    });
}

function characterList(char, insert) {

    const charSet = document.createElement('ul');
    insert.append(charSet);

    char.forEach(item => {
        const charRequest = new XMLHttpRequest();
        charRequest.open('GET', item);
        charRequest.responseType = 'json';
        charRequest.send();

        charRequest.onload = (() => {
            if (charRequest.status !== 200) {
                console.log(`Error: ${charRequest.status}: ${charRequest.statusText}`);
            } else {
                const charResult = charRequest.response.name;
                const charLi = document.createElement('li');
                charLi.innerText = `Name: ${charResult}`;
                charSet.append(charLi);
            }
        })
    });
}