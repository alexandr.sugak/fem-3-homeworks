// // var burgerKing = {
// //     SIZE_SMALL: {
// //         price: 50,
// //         cal: 20,
// //     },
// //
// //     SIZE_LARGE: {
// //         price: 100,
// //         cal: 40,
// //     },
// //
// //     STUFFING_CHEESE: {
// //         price: 10,
// //         cal: 20,
// //     },
// //
// //     STUFFING_SALAD: {
// //         price: 20,
// //         cal: 5,
// //     },
// //
// //     STUFFING_POTATO: {
// //         price: 15,
// //         cal: 10,
// //     },
// //     TOPPING_MAYO: {
// //         price: 20,
// //         cal: 5
// //     },
// //
// //     TOPPING_SPICE: {
// //         price: 15,
// //         cal: 0
// //     }
// // };
//
// // function Hamburger(size, stuffing, menu = burgerKing) {
// //     this._menu = menu;

class Hamburger {
    constructor(size, stuffing) {
        this._menu = {
            SIZE_SMALL: {
                price: 50,
                cal: 20,
            },

            SIZE_LARGE: {
                price: 100,
                cal: 40,
            },

            STUFFING_CHEESE: {
                price: 10,
                cal: 20,
            },

            STUFFING_SALAD: {
                price: 20,
                cal: 5,
            },

            STUFFING_POTATO: {
                price: 15,
                cal: 10,
            },
            TOPPING_MAYO: {
                price: 20,
                cal: 5
            },

            TOPPING_SPICE: {
                price: 15,
                cal: 0
            }
        };

        this.menuList = Object.getOwnPropertyNames(this._menu);

        try {
            if (!this.menuList.includes(size) || !size.includes('SIZE')) {
                throw new Error("Wrong burger size");
            } else {
                this._size = size;
            }
        } catch (error) {
            console.error(`${error.name}: ${error.message}`);
            HamburgerException(error.message);
        }
        try {
            if (!this.menuList.includes(stuffing) || !stuffing.includes('STUFFING')) {
                throw new Error("Wrong burger stuffing");
            } else {
                this._stuffing = stuffing;
            }
        } catch (error) {
            console.error(`${error.name}: ${error.message}`);
            HamburgerException(error.message);
        }
        this._topping = [];

    }

    set addTopping(topping) {
        try {
            if (!this.menuList.includes(topping) || !topping.includes('TOPPING')) {
                throw new Error("Wrong burger topping");
            }
        } catch (error) {
            console.error(`${error.name}: ${error.message}`);
            HamburgerException(error.message);
        }
        try {
            if (this._topping.includes(topping)) {
                throw new Error(topping + " is already on");
            } else {
                this._topping.push(topping);
            }
        } catch (error) {
            console.error(`${error.name}: ${error.message}`);
            HamburgerException(error.message);
        }
    }

    set removeTopping(topping) {
        try {
            if (this._topping.includes(topping)) {
                this._topping.splice(this._topping.indexOf(topping), 1);
            } else {
                throw new Error(topping + ' does not exist in the burger');
            }
        } catch (error) {
            console.error(`${error.name}: ${error.message}`);
            HamburgerException(error.message);
        }
    }

    get toppings() {
        return this._topping;
    };

    get size() {
        return this._size;
    };

    get stuffing() {
        return this._stuffing
    };

    calculatePrice() {
        let price = 0;
        price += this._menu[this._size].price;
        price += this._menu[this._stuffing].price;
        this._topping.forEach(item => price += this._menu[item].price);
        return price;
    };

    calculateCalories() {
        let calories = 0;
        calories += this._menu[this._size].cal;
        calories += this._menu[this._stuffing].cal;
        this._topping.forEach(item =>  calories += this._menu[item].cal);
        return calories;
    };
}


const ErrorMessages = [];

function HamburgerException(message) {
    ErrorMessages.push(message);
}

var ham = new Hamburger('SIZE_LARGE', 'STUFFING_CHEESE');
ham.addTopping = 'TOPPING_MAYO';
ham.addTopping = 'TOPPING_SPICE';
// ham.removeTopping = 'TOPPING_MAYO';

console.log(ham);
console.log('Errors: ' + ErrorMessages);
console.log('Burger size: ' + ham.size);
console.log('Burger stuffing: ' + ham.stuffing);
console.log('Toppings: ' + ham.toppings.join(', '));
console.log('Burger price: ' + ham.calculatePrice());
console.log('Burger calories: ' + ham.calculateCalories());