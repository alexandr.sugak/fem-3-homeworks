class HitAMole {
    constructor() {
        this._tableLayout = [];
        this._startBtn = document.getElementById('new-game-btn');
        this._rows = 10;
        this._cols = 10;
        this._table = document.getElementById('table');
        this._radio = document.querySelectorAll('#game-settings input[name=difficulty]');
        this._humanScore = document.getElementById("human-score");
        this._computerScore = document.getElementById("computer-score");
        this._resultWindow = document.getElementById('result');

        this._humanCounter = 0;
        this._computerCounter = 0;
        this._humanScore.innerText = String(this._humanCounter);
        this._computerScore.innerText = String(this._computerCounter);
        this._difficulty = 1500;
        this._turnsNumber = 1;
        this._turnHumanWinner = false;
        this._activeCell = document.getElementById('table');

        this.start()
    }

    start() {
        this._startBtn.addEventListener('click', () => {
            this._startBtn.disabled = true;
            this._resultWindow.style.display = "none";
            this.startNewGame()
        });
    }

    createTable() {
        const rowInput = document.getElementById('table-width').value;
        const colInput = document.getElementById('table-height').value;

        !isNaN(+rowInput) && +rowInput > 2 ? this._rows = +rowInput : this._rows = 3;
        !isNaN(+colInput) && +colInput > 2 ? this._cols = +colInput : this._cols = 3;

        const newTableArray = [];
        this._table.innerHTML = '';

        for (let i = 1; i <= this._rows; i++) {
            const row = document.createElement('tr');
            row.classList.add(`row-${i}`);
            this._table.append(row);
            for (let j = 1; j <= this._cols; j++) {
                const col = document.createElement('td');
                col.classList.add(`col-${j}`);
                row.append(col);
                newTableArray.push(`.row-${i}>.col-${j}`);
            }
        }
        return newTableArray;
    }

    randomCell() {
        const randomNumber = Math.floor(Math.random() * this._tableLayout.length);
        const result = this._tableLayout[randomNumber];
        this._tableLayout.splice(randomNumber, 1);
        return result;
    }

    checkDifficulty() {
        let result;
        this._radio.forEach(element => {
            if (element.checked) {
                if (element.getAttribute('id') === 'difficulty-easy') {
                    result = 1500;
                }
                if (element.getAttribute('id') === 'difficulty-medium') {
                    result = 1000;
                }
                if (element.getAttribute('id') === 'difficulty-hard') {
                    result = 500;
                }
            }
        });
        return result;
    }

    startNewGame() {
        this._tableLayout = this.createTable();
        this._turnsNumber = this._tableLayout.length / 2;
        this._difficulty = this.checkDifficulty();
        this._humanCounter = 0;
        this._computerCounter = 0;
        this._humanScore.innerText = String(this._humanCounter);
        this._computerScore.innerText = String(this._computerCounter);

        this.gameLoop();
    }

    gameLoop() {
        this._activeCell = document.querySelector(this.randomCell());
        this._activeCell.classList.add('active-cell');
        this._turnHumanWinner = false;

        const humanHit = () => {
            this._turnHumanWinner = true;
            this._activeCell.classList.replace('active-cell', 'clicked-cell');
            this._humanCounter++;
            this._humanScore.innerText = String(this._humanCounter);
            this._activeCell.removeEventListener('click', humanHit);
        };

        this._activeCell.addEventListener('click', humanHit);

        setTimeout(() => {
            if (!this._turnHumanWinner) {
                this._activeCell.classList.replace('active-cell', 'failed-cell');
                this._computerCounter++;
                this._computerScore.innerText = String(this._computerCounter);
            }
            this._activeCell.removeEventListener('click', humanHit);

            if (this._tableLayout.length <= this._turnsNumber) {
                this.gameResult()
            } else {
                this.gameLoop()
            }
        }, this._difficulty);
    }

    gameResult() {
        if (this._humanCounter > this._computerCounter) {
            this._resultWindow.innerHTML = "<p>Game over</p><p>You win!</p>";
        } else if (this._humanCounter < this._computerCounter) {
            this._resultWindow.innerHTML = "<p>Game over</p><p>You lose!</p>";
        } else {
            this._resultWindow.innerHTML = "<p>Game over</p><p>Draw!</p>";
        }
        this._resultWindow.style.display = "block";
        this._startBtn.disabled = false;
    }
}

new HitAMole();
