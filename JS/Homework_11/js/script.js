const buttonWrapper = document.querySelector('.btn-wrapper');

document.body.addEventListener('keyup', (event) => {
    const pressed = event.key[0].toUpperCase() + event.key.slice(1);
    for (let i = 0; i < buttonWrapper.children.length; i++){
        if(pressed === buttonWrapper.children[i].innerHTML) {
            for (let j = 0; j < buttonWrapper.children.length; j++){
                if (buttonWrapper.children[j].classList.contains('pressed')) {
                    buttonWrapper.children[j].classList.remove('pressed');
                }
            }
            buttonWrapper.children[i].classList.add('pressed');
        }
    }
});