function isNumeric(n) {
    return !isNaN(parseInt(n)) && isFinite(n);
}

let firstNumber = 0,
    secondNumber = 0,
    operation = "";
let result;

do {
    firstNumber = +prompt("Enter first number: ", firstNumber);
} while (!isNumeric(firstNumber));
do {
    secondNumber = +prompt("Enter first number: ", secondNumber);
} while (!isNumeric(secondNumber));
do {
    operation = prompt("Enter operation '*' or '+' or '-' or '/'", operation); }
while (operation !== '*' && operation !== '+' && operation !== '-' && operation !== '/');

calculator(firstNumber, secondNumber, operation);

function calculator(firstNumber, secondNumber, operation) {
    switch (operation) {
        case "+":
            result = firstNumber + secondNumber;
            break;
        case "-":
            result = firstNumber - secondNumber;
            break;
        case "*":
            result = firstNumber * secondNumber;
            break;
        case "/":
            result = firstNumber / secondNumber;
            break;
    }
    alert((`${firstNumber} ${operation} ${secondNumber} = ${result}`));
}


