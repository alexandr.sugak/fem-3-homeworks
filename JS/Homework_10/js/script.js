const firstPassIcon = document.getElementById('icon-pass-first');
const secondPassIcon = document.getElementById('icon-pass-second');
const submitBtn = document.querySelector('.btn');
const passForm = document.querySelector(".password-form");
const inputFirst = document.getElementById('input-first');
const inputSecond = document.getElementById('input-second');
const warning = document.querySelector('.warning');

passForm.addEventListener('click', (event) => {
    event.stopPropagation();
    const targetIcon = event.target;
    const inputPass = targetIcon.previousElementSibling;
    if(targetIcon === firstPassIcon || targetIcon === secondPassIcon){
        if (targetIcon.classList.contains('fa-eye')){
            targetIcon.classList.replace('fa-eye', 'fa-eye-slash');
            inputPass.setAttribute('type', 'password');
        } else {
            targetIcon.classList.replace('fa-eye-slash', 'fa-eye');
            inputPass.setAttribute('type', 'text');
        }
    }
    if(targetIcon === submitBtn){
        if(inputFirst.value === inputSecond.value) {
            warning.style.display = 'none';
            alert('You are welcome');
        } else {
            warning.style.display = 'block';
        }
    }
});