function isNumeric(n) {
    return !isNaN(parseInt(n)) && isFinite(n);
}

let number = +prompt("Enter number");

while (!isNumeric(number)) {
    number = +prompt("Enter number", number);
}

function factorial(number) {
    if(number === 1 || number === 0) return 1;
    return number * factorial(number-1);
}

alert(`Factorial from ${number} is: ${factorial(number)}`);
