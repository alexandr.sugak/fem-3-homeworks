// const myArray = ['Kyiv', 'Kharkiv', 'Odesa', 'Lviv', 'Chernihiv', 'Mykolaiv'];
//
// const createNodes = array =>{
//     const createUl = document.createElement('ul');
//     const nodeArray = array.map(element => {
//         const createLi = document.createElement('li');
//         createLi.innerText = element;
//         return createLi;
//     });
//     nodeArray.forEach(element =>{
//         createUl.append(element);
//     });
//     document.querySelector('script').before(createUl);
// };
// createNodes(myArray);

const myArray = ['Kyiv', ['Obolon', 'Pechersk'], 'Kharkiv', 'Odesa', 'Lviv', ['west', 'north', [0, 1, 2, 3], 'east', 'south'], 'Chernihiv', 'Mykolaiv'];

const createNodes = (array, parent) => {
    const createUl = document.createElement('ul');
    if (parent === undefined) {
        document.querySelector('script').before(createUl);
    } else {
        parent.append(createUl);
    }
    array.map((element) => {
        const createLi = document.createElement('li');
        createUl.append(createLi);
        if (typeof element === 'object') {
            createLi.style.listStyle = 'none';
            createNodes(element, createLi);
        } else {
            createLi.innerText = element;
        }
    });
};

createNodes(myArray);

let counterNumbers = document.createElement('p');
counterNumbers.style.cssText = 'font-size: 30px; margin-left: 50px';
document.querySelector('ul').after(counterNumbers);
const show = document.querySelector('ul');
show.style.visibility = 'visible';

let counter = 6;
setInterval(() => {
    counter -= 1;
    counterNumbers.textContent = counter;
    if (counter === 0) {
        if (show.style.visibility === 'visible') {
            show.style.visibility = 'hidden';
        }
        else {
            show.style.visibility = 'visible';
        }
        counter = 6;
    }
}, 1000);