// function isNumeric(n) {
//     return !isNaN(parseInt(n)) && isFinite(n);
// }

// let userNumber;
// do {
//     userNumber = prompt("Enter your number: ", "0");
// } while (!isNumeric(userNumber) || (userNumber % 1 !== 0));
//
// if (userNumber >= 5){
//     for (let i = 0; i <= userNumber; i += 5){
//         console.log(i);
//     }
// } else {
//     console.log("Sorry, no numbers");
// }

// ------------------ Prime Number ------------------

function isNumeric(n, m) {
    return (!isNaN(parseInt(n)) && isFinite(n) && !isNaN(parseInt(m)) && isFinite(m));
}

let numberN = +prompt("Enter your first number: ", "0");
let numberM = +prompt("Enter your second number: ", "0");
while (!(isNumeric(numberN, numberM))|| numberN % 1 !== 0 || numberM % 1 !== 0 || numberN >= numberM) {
    alert("Try again!");
    numberN = +prompt("Enter your first number: ", "0");
    numberM = +prompt("Enter your second number: ", "0");
}
let numberStart = numberN;
if (numberStart <= 2) {
    numberStart = 3;
    console.log(2);
}
for (let i = numberStart; i <= numberM; i++){
    if (i % 2 === 0) continue;
    let prime = true;
    for (let j = 3; j < i; j += 2){ //j += 2
        if (i % j === 0) {
            prime = false;
            break;
        }
    }
    if (prime === false) continue;
    console.log(i);
}
