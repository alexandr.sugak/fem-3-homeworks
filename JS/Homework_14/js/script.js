$(".tabs").on("click","li",function(){
    $(".tabs>.active, .tabs-content>.active").removeClass("active");
    $(this).addClass("active");
    const index = $(this).index();
    $(".tabs-content>li").eq(index).addClass("active");
});

//              -= old version =-
// const $tabs = $(".tabs");
// const $content = $(".tabs-content");
//
// $tabs.on("click","li",  function(event){
//     $tabs.find(".active").removeClass("active");
//     $(this).addClass("active");
//     $content.find(".active").removeClass("active");
//     const index = $(this).index();
//     $content.children().eq(index).addClass("active");
// });
