const myArray = [15, 0, 'hello', 'world', 30, 1, '!', null];

function filterBy(array, type){
    return array.filter(element => {
        if(typeof element !== type) return true;
    })
}

console.log(filterBy(myArray, 'number'));