const themeButton = document.querySelector('.change-theme');
const mainContent = document.querySelector('.main-wrapper');
const headerNav = document.querySelectorAll('.header-navbar-link');

function toDark() {
    mainContent.classList.add('dark-theme');
    themeButton.classList.add('dark-nav');
    headerNav.forEach((element) => {
        element.classList.add('dark-nav');
    });
}
function toLight() {
    mainContent.classList.remove('dark-theme');
    themeButton.classList.remove('dark-nav');
    headerNav.forEach((element) => {
        element.classList.remove('dark-nav');
    });
}

if(localStorage.getItem('switch')){
    toDark()
}

themeButton.addEventListener('click', () => {
    if (localStorage.getItem('switch')) {
        toLight();
        localStorage.removeItem('switch');
    } else {
        toDark();
        localStorage.setItem('switch', 'on');
    }
});