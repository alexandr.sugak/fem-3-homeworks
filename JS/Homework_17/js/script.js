function fibonacci(f0, f1, n) {
    if (n === 0) return 0;
    if (n > 0){
        if (n < 3 ) return 1;
        if (n >= 3){
            n = n-2;
            let f2;
            for (; n > 0; n--){
                f2 = f0 + f1;
                f0 = f1;
                f1 = f2;
            }
            return f2;
        }
    }
    if (n < 0){
        if (n === -1) return 1;
        if (n === -2) return -1;
        if (n <= -3) {
            let even;
            (n % 2) === 0 ? even = true : even = false;
            n *= -1;
            n = n - 2;
            let f2;
            for (; n > 0; n--) {
                f2 = f0 + f1;
                f0 = f1;
                f1 = f2;
            }
            if (even) f2 *= -1;
            return f2;
        }
    }
}

let number = +prompt("Enter number: ", "5");
alert(fibonacci(1, 1, number));
