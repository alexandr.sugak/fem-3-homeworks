const imagesWrapper = document.querySelector('.images-wrapper');
const pauseBtn = document.getElementById('pause-btn');
const continueBtn = document.getElementById('continue-btn');
const timerBlock = document.getElementById('timer');
let currentImage = 0;
imagesWrapper.children[currentImage].classList.add('current-image');
let timerSecond = 5;

function switcher() {
    timerBlock.innerText = `${timerSecond.toFixed(2)}`;
    timerSecond = (timerSecond - 0.01);
    if (timerSecond < 0) {
        imagesWrapper.children[currentImage].classList.remove('current-image');
        if (currentImage === imagesWrapper.children.length - 1) {
            currentImage = 0;
        } else {
            currentImage++;
        }
        imagesWrapper.children[currentImage].classList.add('current-image');
        timerSecond = 5;
        console.log(currentImage);
    }
}

let imageSwitch = setInterval(switcher,10);

pauseBtn.addEventListener("click", () => {
    clearInterval(imageSwitch);
    continueBtn.style.display = 'inline-block';
    pauseBtn.style.display = 'none';
});
continueBtn.addEventListener("click", () => {
    imageSwitch = setInterval(switcher, 10);
    continueBtn.style.display = 'none';
    pauseBtn.style.display = 'inline-block';
});

//                               Простой вариант
// function switcher() {
//     imagesWrapper.children[currentImage].classList.remove('current-image');
//     if (currentImage === imagesWrapper.children.length - 1) {
//         currentImage = 0;
//     } else {
//         currentImage++;
//     }
//     imagesWrapper.children[currentImage].classList.add('current-image');
//     console.log(currentImage);
// }
//
// let imageSwitch =  setInterval(switcher, 5000);