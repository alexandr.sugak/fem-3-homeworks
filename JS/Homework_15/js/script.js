$(".top-header-navbar").on("click","a", function (event) {
    event.preventDefault();
    const id = $(this).attr('href');
    const top = $(id).offset().top;
    $('body, html').animate({scrollTop: top}, 1000);
});

const $topButton = $(".top-button");
const $windowHeight =  $(window).innerHeight();

$(document).scroll(function () {
    if($(window).scrollTop() > $windowHeight){
        $topButton.fadeIn(300, () => {
            $topButton.css("display", "block");
        });
    } else {
        $topButton.fadeOut(300, () => {
            $topButton.css("display", "none");
        });
    }
});

$topButton.click(() => {
    $('body, html').animate({scrollTop: 0}, 1000);
});

const $sliderButton = $(".most-popular__slide");
$sliderButton.click(() => {
    $(".most-popular-images").slideToggle(1000);
    if ($sliderButton.text() === "OPEN") {
        $sliderButton.text("CLOSE");
    } else {
        $sliderButton.text("OPEN");
    }
});