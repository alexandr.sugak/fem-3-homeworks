function isNumeric(n) {
    return !isNaN(parseInt(n)) && isFinite(n);
}
function isString(x) {
    return /^[a-zа-яA-ZА-Я\s]+$/.test(x);
}

let userName = "John",
    userAge = 18;
do{
    userName = prompt("Enter your name: ", userName).trim();
} while (!isString(userName));
do {
    userAge = prompt("Enter your age: ", userAge);
} while (!isNumeric(userAge) || parseInt(userAge) < 1);

if (userAge < 18){
    alert("You are not allowed to visit this website")
} else if (userAge >= 18 && userAge <= 22){
    let result = confirm("Are you sure you want to continue?");
    if (result){
        alert("Welcome, " + userName);
    } else {
        alert("You are not allowed to visit this website");
    }
} else {
    alert("Welcome, " + userName);
}