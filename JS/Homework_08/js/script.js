const priceWrapper = document.createElement('div');
document.querySelector('script').before(priceWrapper);
priceWrapper.style.cssText = "width: 380px; height: 50px; background-color: #e94641; font-size: 20px; line-height: 50px;";
const priceText = document.createElement('label');
priceWrapper.append(priceText);
priceText.innerText = "Price: ";
priceText.style.cssText = "color: #ffffff; padding-left: 50px;";
const priceInput = document.createElement('input');
priceText.append(priceInput);
priceInput.style.cssText = "border-radius: 4px; color: #343434; height: 20px; font-size: 16px; outline:none; text-indent:10px";
const currentPrice = document.createElement('span');
priceWrapper.before(currentPrice);
currentPrice.style.cssText = "border-radius: 50px 50px 50px 50px; border: solid grey 1px;  color: grey; height: 20px; font-size: 16px; padding: 5px 15px 5px; margin: 20px 5px 20px 20px; display: inline-block; visibility: hidden;";
const close = document.createElement('span');
currentPrice.after(close);
close.innerText = "x";
close.style.cssText = "border-radius: 50%; border: solid grey 1px;  color: grey; height: 20px; font-size: 16px; padding: 5px 10px; display: inline-block; visibility: hidden; font-family: sans-serif";


priceInput.addEventListener('focus', () =>{
    priceInput.style.border = "solid limegreen 2px";
});
priceInput.addEventListener('focusout', () => {
    if (Number(priceInput.value) < 0 || isNaN(+priceInput.value) || priceInput.value === "") {
        if (!document.getElementById('error')) {
            const error = document.createElement('span');
            error.id = "error";
            priceWrapper.before(error);
            error.innerText = "Please enter correct price";
        }
        priceInput.style.border = "solid red 2px";
        currentPrice.style.visibility = "hidden";
        close.style.visibility = "hidden";
    } else {
        currentPrice.textContent = `Current price: \$ ${priceInput.value}`;
        currentPrice.style.visibility = "visible";
        close.style.visibility = "visible";
        priceInput.style.border = "none";
        priceInput.style.color = "limegreen";
        if (document.getElementById('error')) document.getElementById('error').remove();
    }
});
close.addEventListener('click', () =>{
    currentPrice.style.visibility = "hidden";
    close.style.visibility = "hidden";
    priceInput.value = "";
});
