const createNewUser = function(){
    const newUser = {
        firstName: prompt("Enter your first name: ", "Ivan"),
        lastName: prompt("Enter your last name: ", "Petrov"),
        birthday: prompt("Enter your birthday (dd.mm.yyyy): ", "20.12.1999"),
        getLogin(){
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge(){
            const parsedDate = this.birthday.split(".");
            const birthDate = new Date(`${parsedDate[1]}-${parsedDate[0]}-${parsedDate[2]}`);
            const today = new Date();
            let age = today.getFullYear() - birthDate.getFullYear();
            const m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
        },
        getPassword(){
            let year = ""; // Это первое к чему додумался, а потом уже был substr, поэтому решил так и оставить
            for (let i = this.birthday.length - 4; i < this.birthday.length; i++){
                year += this.birthday[i];
            }
            return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + year);
        }
    };
    return newUser;
};

const user = createNewUser();
console.log(user.getAge());
console.log(user.getPassword());



