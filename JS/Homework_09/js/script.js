const navTabs = document.querySelector('.tabs');
const content = document.querySelector('.tabs-content');


navTabs.addEventListener('click', (event) => {
    const activeTab = document.querySelector('.tabs>li.active');
    if (activeTab) activeTab.classList.remove('active');
    const contentText = document.querySelector('.tabs-content>li.active');
    if (contentText) contentText.classList.remove('active');

    const currentTab = event.target;
    currentTab.classList.add("active");
    let index;
    for (let i = 0; i < navTabs.children.length; i++) {
        if (navTabs.children[i] === currentTab) {
            index = i;
            break;
        }
    }
    content.children[index].classList.add('active');
});

