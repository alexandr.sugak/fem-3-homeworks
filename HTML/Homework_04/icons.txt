icon-forum-hover, .icon-forum, .icon-google, .icon-journey-hover, .icon-journey, 
.icon-calendar-hover, .icon-calendar, .icon-facebook
{ display: inline-block; background: url('png.png') no-repeat; overflow: hidden; text-indent: -9999px; text-align: left; }

.icon-forum-hover { background-position: -1px -0px; width: 28px; height: 20px; }
.icon-forum { background-position: -1px -21px; width: 28px; height: 20px; }
.icon-google { background-position: -1px -42px; width: 25px; height: 16px; }
.icon-journey-hover { background-position: -1px -59px; width: 20px; height: 20px; }
.icon-journey { background-position: -1px -80px; width: 20px; height: 20px; }
.icon-calendar-hover { background-position: -1px -101px; width: 19px; height: 19px; }
.icon-calendar { background-position: -1px -121px; width: 19px; height: 19px; }
.icon-facebook { background-position: -1px -141px; width: 10px; height: 20px; }